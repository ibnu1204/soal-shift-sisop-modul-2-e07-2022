# soal-shift-sisop-modul-2-E07-2022
### Anggota Kelompok E07
|Nama|NRP|
|--------------|--------------|
|Tegar Ganang Satrio Priambodo|5025201002|
|Rafael Asi Kristanto Tambunan|5025201168|
|Ahmad Ibnu Malik Rahman|5025201232|

# Soal 1
# Soal 2
# Soal 3
Conan adalah seorang detektif terkenal. Suatu hari, Conan menerima beberapa laporan tentang hewan di kebun binatang yang tiba-tiba hilang. Karena jenis-jenis hewan yang hilang banyak, maka perlu melakukan klasifikasi hewan apa saja yang hilang
a. Untuk mempercepat klasifikasi, Conan diminta membuat program untuk membuat 2 directory di “/home/[USER]/modul2/” dengan nama “darat” lalu 3 detik kemudian membuat directory ke 2 dengan nama “air”. 
b. Kemudian program diminta dapat melakukan extract “animal.zip” di “/home/[USER]/modul2/”.
c. Tidak hanya itu, hasil extract dipisah menjadi hewan darat dan hewan air sesuai dengan nama filenya. Untuk hewan darat dimasukkan ke folder “/home/[USER]/modul2/darat” dan untuk hewan air dimasukkan ke folder “/home/[USER]/modul2/air”. Rentang pembuatan antara folder darat dengan folder air adalah 3 detik dimana folder darat dibuat terlebih dahulu. Untuk hewan yang tidak ada keterangan air atau darat harus dihapus.
d. Setelah berhasil memisahkan hewan berdasarkan hewan darat atau hewan air. Dikarenakan jumlah burung yang ada di kebun binatang terlalu banyak, maka pihak kebun binatang harus merelakannya sehingga conan harus menghapus semua burung yang ada di directory “/home/[USER]/modul2/darat”. Hewan burung ditandai dengan adanya “bird” pada nama file.
e. Terakhir, Conan harus membuat file list.txt di folder “/home/[USER]/modul2/air” dan membuat list nama semua hewan yang ada di directory “/home/[USER]/modul2/air” ke “list.txt” dengan format UID_[UID file permission]_Nama File.[jpg/png] dimana UID adalah user dari file tersebut file permission adalah permission dari file tersebut.
Contoh : conan_rwx_hewan.png

Catatan :
- Tidak boleh memakai system().
- Tidak boleh memakai function C mkdir() ataupun rename().
- Gunakan exec dan fork
- Direktori “.” dan “..” tidak termasuk

## Penyelesaian

a. Untuk mempercepat klasifikasi, Conan diminta membuat program untuk membuat 2 directory di “/home/[USER]/modul2/” dengan nama “darat” lalu 3 detik kemudian membuat directory ke 2 dengan nama “air. 

pada sub soal ini praktikan diminta untuk membuat 2 directory. akan tetapi tidak diperbolehkan untuk menggunakan fungsi mkdir() dan harus menggunakan execv. Pada modul 2 sebelum menggunakan execv kita perlu melakukan fork untuk menjalankan proses baru. 

pada kode berikut digunakan nested if dimana setiap peralihan nested if memerlukan status fork() dari if sebelumnya agar dapat melakukan fork() untuk proses selanjutnya
```
int main(){
  pid_t child_id;
  int status = 0;
  child_id = fork();

  if(child_id < 0)
    exit(EXIT_FAILURE);
  
  else if (child_id == 0) {
    char *argv[] = {"mkdir", "-p", "darat", NULL};
    execv("/bin/mkdir", argv);
  } 

  else {
    pid_t child_id2;
    int status2 = 0;
    child_id2 = fork();
    while ((wait(&status)) > 0);
    sleep(3);

    if (child_id2 == 0){
      char *argv[] = {"mkdir", "-p", "air", NULL};
      execv("/bin/mkdir", argv);
    }
    
    else {
      pid_t child_id3;
      int status3 = 0;
      child_id3 = fork();
      if(child_id3 == 0){
        unzip();
      }
      
      else{
        int status4 = 0;
        while(wait(&status3) > 0);
        if(fork() == 0){
          char pathAir[] = "/home/ibnumalik/modul2/air";
          char air[] = "*air*";
          char pathDarat[] = "/home/ibnumalik/modul2/darat";
          char darat[] = "*darat*";

          move(pathAir, air);
          move(pathDarat, darat);
          removeFile();
        }
        
        else {
          int status5 = 0;
          while(wait(&status4) > 0);
          if(fork() == 0){
            removeBird();
          }
          
          else {
            while(wait(&status5) > 0);
            list();
          }
        }
      }
    }
  }
}
```
pada soal diminta ketika melakukan pembuatan directory darat 3 detik kemudian dilakukan pembuatan directory air dengan fungsi sleep()

Berikut adalah hasilnya setelah program dijalankan

![3a](/uploads/a45170c8333d651ab23c3821e81e6b3f/3a.png)

b. Kemudian program diminta dapat melakukan extract “animal.zip” di “/home/[USER]/modul2/”.

untuk melakukan extract file animal.zip atau biasa disebut unzip dapat dilakukan dengan kode seperti ini

```
void unzip(){
  int status = 0;
  if(fork()==0){
   	char *argv[] = {"unzip", "-q", "-o", "/home/ibnumalik/modul2/animal.zip", "-d", "/home/ibnumalik/modul2", NULL};
    execv("/bin/unzip", argv);
  }
  while(wait(&status)>0); 
}
```
c. Tidak hanya itu, hasil extract dipisah menjadi hewan darat dan hewan air sesuai dengan nama filenya. Untuk hewan darat dimasukkan ke folder “/home/[USER]/modul2/darat” dan untuk hewan air dimasukkan ke folder “/home/[USER]/modul2/air”. Rentang pembuatan antara folder darat dengan folder air adalah 3 detik dimana folder darat dibuat terlebih dahulu. Untuk hewan yang tidak ada keterangan air atau darat harus dihapus.

pada soal ini kita diminta untuk memindahkan file gambar sesuai dengan jenis hewan dan juga menghapus file yang bukan merupakan hewan darat, air dan burung.

Untuk memindahkan file sesuai klasifikasinya, kita bisa menggunakan pencarian terlebih dahulu untuk mencari file dengan kriteria yang diberikan. Setelah kita mencari file, selanjutnya dipindahkan kengan menggunakan perintah -mv.

```
void move(char path[], char file[]){
  int status = 0;
  if(fork()==0){
   	char *argv[] = {"find", "/home/ibnumalik/modul2/animal", "-name", file, "-exec", "mv", "-t",path, "{}","+",NULL };
    execv("/bin/find", argv);
  }
  while(wait(&status)>0); 
}
```
Untuk menghapus file yang tidak termasuk kategori darat, air, ataupun burung kita perlu melakukan pencarian kemudian di remove dengan menggunakan perintah rm.

```
void removeFile() {
  int status = 0;
  if(fork()==0) {
    char *argv[] = {"find", "/home/ibnumalik/modul2/animal", "-name", "*.jpg*", "-exec", "rm", "{}","+",NULL };
    execv("/bin/find", argv);
  }
  while(wait(&status)>0); 
}
```
d. Setelah berhasil memisahkan hewan berdasarkan hewan darat atau hewan air. Dikarenakan jumlah burung yang ada di kebun binatang terlalu banyak, maka pihak kebun binatang harus merelakannya sehingga conan harus menghapus semua burung yang ada di directory “/home/[USER]/modul2/darat”. Hewan burung ditandai dengan adanya “bird” pada nama file.

```
void removeBird() {
  int status = 0;
  if(fork()==0) {
   	char *argv[] = {"find", "/home/ibnumalik/modul2/darat", "-name", "*bird*", "-exec", "rm", "{}","+",NULL };
    execv("/bin/find", argv);
  }
  while(wait(&status)>0);  
}
```
Berikut adalah hasil dari soal b, c, dan d

![darat](/uploads/21e7c13b865ccfaf138104ca419260b6/darat.png)

![air](/uploads/f807f824712e7b67634a7075fde3d9a1/air.png)

e. Terakhir, Conan harus membuat file list.txt di folder “/home/[USER]/modul2/air” dan membuat list nama semua hewan yang ada di directory “/home/[USER]/modul2/air” ke “list.txt” dengan format UID_[UID file permission]_Nama File.[jpg/png] dimana UID adalah user dari file tersebut file permission adalah permission dari file tersebut.
Contoh : conan_rwx_hewan.png

nantinya username yang mengakses file disimpan pada pw->pw_name

```
void list(){
  FILE *fptr;
  struct dirent *dt;
  char path[] = "/home/ibnumalik/modul2/air/";
  DIR *dir = opendir(path);

  fptr = fopen("/home/ibnumalik/modul2/air/list.txt", "w");
  if(fptr == NULL) {
    printf("Unable to create file.\n");
    exit(EXIT_FAILURE);
  }

  while ((dt = readdir(dir)) != NULL) {
    struct stat fs;
    int r = stat(path, &fs);

    if(r == -1) {
      fprintf(stderr, "File Error\n");
      exit(1);
    }
    
    register struct passwd *pw;
    uid_t uid = getuid();
    pw = getpwuid(uid);
    if(strcmp(dt->d_name,".") != 0 && strcmp(dt->d_name, "..") != 0 && strcmp(dt->d_name, "list.txt") != 0){
      char userp[256] = "";
      if(fs.st_mode & S_IRUSR)
        strcat(userp, "r");
      if(fs.st_mode & S_IWUSR)
        strcat(userp, "w");
      if(fs.st_mode & S_IXUSR)
        strcat(userp, "x");
      fprintf(fptr,"%s_%s_%s\n",pw->pw_name,userp, dt->d_name);
    }
  }
  fclose(fptr);
}
```
Berikut hasilnya pada file `list.txt`

![list](/uploads/db5be14a1fe4bb0e7870b80adf064e96/list.png)



